<?php

use yii\db\Migration;

class m160301_135650_kecamatan extends Migration
{
    public function safeUp()
    {
        $this->createTable("kecamatan", [
            "id"      	 	=> "int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY",
            "id_kota"       => "int(10) unsigned NOT NULL",
            "nama"     		=> "varchar(20) COLLATE utf16_unicode_ci NOT NULL",
            "status"       	=> "int(11) NOT NULL DEFAULT '1' COMMENT '1=tampil,2=delete'",
        ]);
        $this->addForeignKey('kecamatan_ibfk_1', 'kecamatan', 'id_kota', 'kota', 'id','RESTRICT','RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable("kecamatan");
    }
}
