<?php

use yii\db\Migration;

class m160301_134953_kota extends Migration
{
    public function safeUp()
    {
		 $this->createTable('kota', [
            'id'      	 	=> 'int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'kode'       	=> 'varchar(15) NOT NULL',
            'nama'     		=> 'varchar(20) NOT NULL',
            'status'       	=> "int(11) NOT NULL DEFAULT '1' COMMENT '1=tampil,2=delete'",
        ]);
    }

    public function safeDown()
    {
		$this->dropTable('kota');
    }
}
