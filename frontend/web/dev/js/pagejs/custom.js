function format_angka(angka) {
    var components = angka.toString().split(".");
    components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return components.join(".");
}

function reformat_angka(angka) {
    var components = angka.toString().split(",");
    components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return components.join("");
}

$(".chosen-select").chosen({});

$('#tanggal_berangkat').datepicker({
		todayHighlight: false,
		format: 'dd/mm/yyyy',
		autoclose:true,
});  




