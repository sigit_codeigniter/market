$("#kota_asal").change(function(){
    var id_jurusan_asal = $(this).val();
    $.ajax({
        url: '/site/getdatatujuan',
        type: 'get',
        dataType:'json',
        data: {id_jurusan_asal:id_jurusan_asal},
        success: function(data) {
            var txt = "";
            txt += "<option value=''>- Pilih Kota -</option>";
            $.each(data, function(index, val) {
                txt += "<option value='"+data[index].id+"'>"+data[index].nama+"</option>";
            });
            $("#kota_tujuan").html(txt);
            $("#kota_tujuan").trigger("chosen:updated");
        }
    });
    return false;
});