function format_angka(angka) {
    var components = angka.toString().split(".");
    components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return components.join(".");
}

function reformat_angka(angka) {
    var components = angka.toString().split(",");
    components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return components.join("");
}

$(".chosen-select").chosen({});

$('#tanggal_berangkat').datepicker({
		todayHighlight: false,
		format: 'dd/mm/yyyy',
		autoclose:true,
});
$(document).ready(function () {
    try {
        var markers = [
            {
                "title": 'Aksa Beach',
                "lat": '-7.972157',
                "lng": '112.64285',
                "description": 'King Travel'
            },
            {
                "title": 'Aksa Beach',
                "lat": '-7.9995114',
                "lng": '112.6424361',
                "description": 'Zena Travel'
            },
            {
                "title": 'Aksa Beach',
                "lat": '-7.915233',
                "lng": '112.609243',
                "description": 'Transuperindo Travel'
            },
            {
                "title": 'Aksa Beach',
                "lat": '-7.9557023',
                "lng": '112.6061248',
                "description": 'Trans Express2 Travel'
            },
        ];
        var mapOptions = {
            center: new google.maps.LatLng(-7.9784695,112.561742),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-vendor"), mapOptions);

        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow();

        for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });

            //Attach click event to the marker.
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
    }catch (err){
        console.log(err.message)
    }
});


