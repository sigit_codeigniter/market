function format_angka(angka) {
    var components = angka.toString().split(".");
    components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return components.join(".");
}

function reformat_angka(angka) {
    var components = angka.toString().split(",");
    components [0] = components [0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    return components.join("");
}

$(".chosen-select").chosen({});

$('#tanggal_berangkat').datepicker({
		todayHighlight: false,
		format: 'dd/mm/yyyy',
		autoclose:true,
});
$(document).ready(function () {
    try {
        var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');
        allWells.hide();
        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                $item = $(this);
            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-warning').addClass('btn-default');
                $item.addClass('btn-default');
                allWells.hide();
                $target.show();
                $target.find('.panel-body').slideDown( "slow" );
                $target.find('input:eq(0)').focus();
            }
        });

        $(".nextData").click(function(){
            var curStep = $(this).closest(".setup-content");
            var curStepBtn = curStep.attr("id");
            var nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a");
            nextStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function(){
            var curStep = $(this).closest(".setup-content"),
                curStepBtn = curStep.attr("id"),
                nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                curInputs = curStep.find("input[type='text'],input[type='url'],textarea[textarea]"),
                isValid = true;

            $(".form-group").removeClass("has-error");
            for(var i=0; i<curInputs.length; i++){
                if (!curInputs[i].validity.valid){
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });
        $('div.setup-panel div a.btn-default').trigger('click');

        //$(".book").click(function(event){
        //    $('html, body').stop().animate({
        //        scrollTop: $(".frm_booking").offset().top
        //    }, 1500, 'easeInOutExpo');
        //    event.preventDefault();
        //});

        $("#add_penumpang").click(function(event){
        });

        function addPenumpang(url){
            alert("sigit");
        }
    }catch (err){
        console.log(err.message)
    }
});
