<?php

namespace frontend\models;

use Yii;

/**
 * ContactForm is the model behind the contact form.
 */
class Trv_jurusan_asal extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'trv_jurusan_asal';
    }

    public function get_asal()
    {
        $findData = Trv_jurusan_asal::find()
            ->select('trv_jurusan_asal.*,kota.nama')
            ->leftJoin('kota','kota.id = trv_jurusan_asal.id_kota')
            ->asArray()
            ->all();
        return $findData;
    }
}
