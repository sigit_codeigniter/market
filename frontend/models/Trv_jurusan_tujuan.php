<?php

namespace frontend\models;

use Yii;

/**
 * ContactForm is the model behind the contact form.
 */
class Trv_jurusan_tujuan extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'trv_jurusan_tujuan';
    }

    public function get_tujuan($id_jurusan_asal)
    {
        $findData = Trv_jurusan_tujuan::find()
            ->select('trv_jurusan_tujuan.*,kota.nama')
            ->leftJoin('kota','kota.id = trv_jurusan_tujuan.id_kota')
            ->where('trv_jurusan_tujuan.id_jurusan_asal = '.$id_jurusan_asal)
            ->asArray()
            ->all();
        return $findData;
    }
}
