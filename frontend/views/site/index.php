<?php
use yii\helpers\Html;
?>
<div class="top"></div>
<div>
<section class="hero hero-overlap hidden-xs">
    <div class="container">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox" style="border-radius:5px 5px 0px 0px;">
            <div class="item active">
                <?php echo Html::img('@web/dev/image/image1.jpg')?>
            </div>
            <div class="item">
                <?php echo Html::img('@web/dev/image/image2.jpg')?>
            </div>
            <div class="item">
                <?php echo Html::img('@web/dev/image/image3.jpg')?>
            </div>
            <div class="item">
                <?php echo Html::img('@web/dev/image/image4.jpg')?>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    </div>
</section>
<section class="featured-destinations">
    <div class="container">
        <div class="cards overlap">
            <!-- Cards Row -->
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div>
                            <h3 style="color:#fff;margin-top: 0px; margin-bottom: 0px; background-color: rgba(0, 0, 0, 0.78); padding: 10px 20px;">Search Travel Vendor's</h3>
                        </div>
                        <article class="card">
                            <div class="card-details">
                                <form action="/cari" method="post">
                                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Kota Asal</label>
                                                        <select id="kota_asal" name="kota_asal" class="form-control chosen-select">
                                                            <option value="">- Pilih Kota -</option>
                                                             <?php foreach($data_asal as $key_asal => $val_asal):?>
                                                            <option value="<?= $val_asal['id']?>"><?= $val_asal['nama']?></option>
                                                            <?php endforeach;?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Kota Tujuan</label>
                                                        <select id="kota_tujuan" name="kota_tujuan" class="form-control chosen-select">
                                                            <option value="">- Pilih Kota -</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Tanggal Berangkat</label>
                                                        <input type="text" class="form-control" id="tanggal_berangkat" placeholder="Tanggal Berangkat">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Jam Berangkat</label>
                                                        <input type="text" class="form-control" id="jam_berangkat" placeholder="Jam Berangkat">
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-default btn-block">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </article>
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.row -->
        </div>
    </div>
</section>
<section class="bs-docs-section clearfix">
    <div class="container" style="padding-bottom: 15px;">
    <div class="row">
        <div class="col-lg-12">
            <div class="bs-component">
                <div class="panel panel-default widgetCS" style="border-radius:0px 0px 5px 5px;">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <!--advertise here-->

                                <!--end of advertise here-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<section class="bs-docs-section clearfix" style="background: #eee none repeat scroll 0 0;">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="bs-component">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h1>WHY US</h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <h3 style="color:#0275d8;font-weight: bold">We're Here for You</h3>
                                <ul style="padding-left: 0px;list-style: none;color:#003580">
                                    <li style="font-size:19px;padding-bottom: 20px;">
                                        <span class="glyphicon glyphicon-usd box-icon-large box-icon-left box-icon-def box-icon-border-dashed round icon-kuning"></span>
                                        Harga Terjangkau
                                        <div class="clearfix"></div>
                                    </li>
                                    <li style="font-size:19px;padding-bottom: 20px;">
                                        <span class="glyphicon glyphicon-glyphicon glyphicon-thumbs-up box-icon-large box-icon-left box-icon-def box-icon-border-dashed round icon-biru"></span>
                                        Fast Response
                                        <div class="clearfix"></div>
                                    </li>
                                    <li style="font-size:19px;padding-bottom: 20px;">
                                        <span class="glyphicon glyphicon-time box-icon-large box-icon-left box-icon-def box-icon-border-dashed round icon-ungu"></span>
                                        Layanan Customer 24 Jam
                                        <div class="clearfix"></div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-6">
                                <h3 style="color:#0275d8;font-weight: bold">Top 5 Travel Vendor</h3>
                                <ul class="b-popular_list lp_endorsements_popular_destinations_container">
                                    <li class="b-popular_item b-sprite-wrap">
                                        <a class="b-popular_thumb_holder pull-left" href="">
                                            <?php echo Html::img('@web/dev/image/payment/paypal.gif')?>
                                        </a>
									<span class="h4 pull-left vendorTitle">
									Travel Vendor
									</span>
                                    </li>
                                    <li class="b-popular_item b-sprite-wrap">
                                        <a class="b-popular_thumb_holder pull-left" href="">
                                            <?php echo Html::img('@web/dev/image/payment/paypal.gif')?>
                                        </a>
									<span class="h4 pull-left vendorTitle">
									Travel Vendor
									</span>
                                    </li>
                                    <li class="b-popular_item b-sprite-wrap">
                                        <a class="b-popular_thumb_holder pull-left" href="">
                                            <?php echo Html::img('@web/dev/image/payment/paypal.gif')?>
                                        </a>
									<span class="h4 pull-left vendorTitle">
									Travel Vendor
									</span>
                                    </li>
                                    <li class="b-popular_item b-sprite-wrap">
                                        <a class="b-popular_thumb_holder pull-left" href="">
                                            <?php echo Html::img('@web/dev/image/payment/paypal.gif')?>
                                        </a>
									<span class="h4 pull-left vendorTitle">
									Travel Vendor
									</span>
                                    </li>
                                    <li class="b-popular_item b-sprite-wrap">
                                        <a class="b-popular_thumb_holder pull-left" href="">
                                            <?php echo Html::img('@web/dev/image/payment/paypal.gif')?>
                                        </a>
									<span class="h4 pull-left vendorTitle">
									Travel Vendor
									</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php
$this->registerJsFile('@web/dev/js/pagejs/custom.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/dev/js/pagejs/index.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>