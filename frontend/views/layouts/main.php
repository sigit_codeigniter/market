<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\TemplateAsset;
use common\widgets\Alert;
use yii\widgets\Pjax;
TemplateAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a href="../" class="navbar-brand">My Blog</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav navbar-right">
                <li><?=Html::a("Home", 'index.php' )?></li>
                <li><a href="">Why Us</a></li>
                <li><a href="">About Us</a></li>
                <li><a href="">Career</a></li>
                <li><a href="">Register</a></li>
            </ul>
        </div>
    </div>
</div>

    <?= $content ?>
    <footer>
        <div class="container">
            <div class="row" style="padding-bottom: 10px;">
                <div class="col-md-4">
                    <p style="padding-top: 10px;" class="h4">Tentang Kami</p>
                <span>
                <ul style="padding-left: 0px;" class="footer-us">
                    <li><a title="Tentang Kami" href="">Tentang Kami</a></li>
                    <li><a title="Karir" href="">Karir</a></li>
                    <li><a title="Contact Us" href="">Contact Us</a></li>
                    <li><a title="Mitra Hotel Login" href="">Mitra Travel Login</a></li>
                </ul>
                </span>
                </div>
                <div class="col-md-4">
                    <p style="padding-top: 10px;" class="h4">Metode Pembayaran</p>
                    <span>
                    Kami menerima pembayaran melalui ATM, Transfer Bank, Perbankan Online, dan Kartu Kredit.
                    <ul class="imageList">
                        <li><span class="spriteFooter img_bayar"><?php echo Html::img('@web/dev/image/payment/paypal.gif')?></span></li>
                        <li><span class="spriteFooter img_bayar"><?php echo Html::img('@web/dev/image/payment/mastercard.gif')?></span></li>
                        <li><span class="spriteFooter img_bayar"><?php echo Html::img('@web/dev/image/payment/visa.gif')?></span></li>
                        <li><span class="spriteFooter img_bayar"><?php echo Html::img('@web/dev/image/payment/discover.gif')?></span></li>
                        <li><span class="spriteFooter img_bayar"><?php echo Html::img('@web/dev/image/payment/americanexpress.gif')?></span></li>
                    </ul>
                    </span>
                </div>
                <div class="col-md-4">

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center" style="border-top: 1px solid #fff;">
                    <p style="padding-top: 20px;padding-bottom: 20px;">Copyright by Raya Travel @ 2016</p>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
