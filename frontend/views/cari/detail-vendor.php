<?php
use yii\helpers\Html;
?>
<div class="top"></div>
<section class="bs-docs-section clearfix">
    <div class="container">
    <div class="row">
        <div class="col-lg-12" id="main">
            <div class="row">
                <div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-body" id="logo-vendor-booking">
                            Logo Vendor Here
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="row">
                        <div class="col-md-12 pad-bottom5">
                            <table class="table table-bordered ">
                                <tr>
                                    <td><span class="text-kuning tebal">Vendor Travel :</span></td>
                                    <td><span class="tebal">Raya Travel</span></td>
                                </tr>
                                <tr>
                                    <td><span class="text-kuning tebal">Rute :</span></td>
                                    <td><span class="tebal">MLG - SUB</span></td>
                                </tr>
                                <tr>
                                    <td><span class="text-kuning tebal">Tanggal :</span></td>
                                    <td> <span class="tebal">22-02-2016</span></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="for-free">
                                <ul id="carIncludesList" class="result_included" style="padding-left: 10px;list-style: none">
                                    <li class="result_included_title">GRATIS:</li>
                                    <li class="result_includes" style="cursor: pointer;">Perubahan</li>
                                    <li class="result_includes" style="cursor: pointer;">Proteksi dari pencuri</li>
                                    <li class="result_includes" style="cursor: pointer;">Asuransi Kerusakan Akibat Kecelakaan</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="stepwizard">
                        <div class="stepwizard-row setup-panel">
                            <div class="stepwizard-step">
                                <a href="#step-1" type="button" class="btn btn-default btn-circle">1</a>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-2" type="button" class="btn btn-warning btn-circle" disabled="disabled">2</a>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-3" type="button" class="btn btn-warning btn-circle" disabled="disabled">3</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-1">
                <div class="col-lg-12">
                    <div class="panel panel-default set-margin-top">
                        <div class="panel-body">
                            <div class="col-lg-12 mb-10">
                                <button id="nextData" class="nextData btn btn-success btn-sm pull-right" type="button" >Selanjutnya <i class="glyphicon glyphicon-arrow-right"></i></button>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-3 pad-topbottom15">
                                                <i class="glyphicon glyphicon-time no-margin-right"></i><span class="tebal">03:00</span>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-3">
                                                <a href="#frm_booking" class="book btn btn-sm btn-default btn-block">Book Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-3 pad-topbottom15">
                                                <i class="glyphicon glyphicon-time no-margin-right"></i><span class="tebal">07:00</span>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-3">
                                                <a href="#frm_booking" class="book btn btn-sm btn-default btn-block">Book Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-3 pad-topbottom15">
                                                <i class="glyphicon glyphicon-time no-margin-right"></i><span class="tebal">07:00</span>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-3">
                                                <a class="book btn btn-sm btn-default btn-block">Book Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-3 pad-topbottom15">
                                                <i class="glyphicon glyphicon-time no-margin-right"></i><span class="tebal">07:00</span>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-3">
                                                <a class="book btn btn-sm btn-default btn-block">Book Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-3 pad-topbottom15">
                                                <i class="glyphicon glyphicon-time no-margin-right"></i><span class="tebal">05:00</span>
                                            </div>
                                            <div class="col-lg-6">
                                            </div>
                                            <div class="col-lg-3">
                                                <a class="book btn btn-sm btn-default btn-block">Book Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <h4>No Schedulle Available</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-2">
                <div class="col-lg-12">
                    <div class="panel panel-default set-margin-top">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 class="text-biru no-margin-top">Data Penumpang</h3>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-dismissible alert-warning pad-topbottom15">
                                        Isi data dibawah dan tekan tambahkan penumpang.
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-user"></i>
                                        <input type="text" placeholder="Nama Pemesan" id="nm_pemesan" class="form-control"  name="nm_pemesan">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-phone"></i>
                                        <input type="text" placeholder="No HP" id="hp_pemesan" class="form-control"  name="hp_pemesan">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <hr class="no-margin-top">
                                </div>
                                <div class="col-lg-3">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-map-marker"></i>
                                        <select id="kota_tujuan" data-placeholder="Kecamatan Asal" name="kecamatan_asal" class="form-control chosen-select">
                                            <option value=""></option>
                                            <option value="">Malang</option>
                                            <option value="">Malang (ABD Saleh)</option>
                                            <option value="">Surabaya</option>
                                            <option value="">Surabaya (Juanda)</option>
                                            <option value="">Sidoarjo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-map-marker"></i>
                                        <select id="kota_tujuan" data-placeholder="Kelurahan Asal" name="kelurahan_asal" class="form-control chosen-select">
                                            <option value=""></option>
                                            <option value="">Malang</option>
                                            <option value="">Malang (ABD Saleh)</option>
                                            <option value="">Surabaya</option>
                                            <option value="">Surabaya (Juanda)</option>
                                            <option value="">Sidoarjo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-map-marker"></i>
                                        <input type="text" placeholder="Alamat Asal" id="alamat_asal" class="form-control"  name="hp_pemesan">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <hr class="no-margin-top">
                                </div>
                                <div class="col-lg-3">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-map-marker"></i>
                                        <select id="kota_tujuan" data-placeholder="Kecamatan Tujuan" name="kecamatan_tujuan" class="form-control chosen-select">
                                            <option value=""></option>
                                            <option value="">Malang</option>
                                            <option value="">Malang (ABD Saleh)</option>
                                            <option value="">Surabaya</option>
                                            <option value="">Surabaya (Juanda)</option>
                                            <option value="">Sidoarjo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-map-marker"></i>
                                        <select id="kota_tujuan" data-placeholder="Kelurahan Tujuan" name="kecamatan_tujuan" class="form-control chosen-select">
                                            <option value=""></option>
                                            <option value="">Malang</option>
                                            <option value="">Malang (ABD Saleh)</option>
                                            <option value="">Surabaya</option>
                                            <option value="">Surabaya (Juanda)</option>
                                            <option value="">Sidoarjo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="left-inner-addon">
                                        <i class="glyphicon glyphicon-map-marker"></i>
                                        <input type="text" placeholder="Alamat Tujuan" id="alamat_tujuan" class="form-control"  name="alamat_tujuan">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="add_penumpang" class="btn btn-sm btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Tambah Penumpang</a>
                                </div>
                            </div>
                            <div class="row set-margin-top">
                                <div class="col-lg-12">
                                    <table class="table table-striped table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama</th>
                                            <th>Alamat Jemput</th>
                                            <th>Alamat Tujuan</th>
                                            <th colspan="2" class="text-center">Opsi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td>Column content</td>
                                            <td class="text-center"><a class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-trash"></i></a></td>
                                            <td class="text-center"><a class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-edit"></i></a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row setup-content" id="step-3">
                <div class="col-lg-12">
                    Kalkulasi Biaya
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php
$this->registerJsFile('@web/dev/js/pagejs/detail_vendor.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>