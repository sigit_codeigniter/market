<?php
use yii\helpers\Html;
?>
<div class="top" style="margin-top:70px;"></div>
<section class="bs-docs-section clearfix">
    <div class="container">
    <div class="row">
        <div class="col-lg-4" id="side">
            <div class="bs-component">
                <div class="panel panel-default">
                    <div class="panel-body" style="background-color: #ffd21c;">
                        <form action="/cari" method="post">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kota Asal</label>
                                                <select id="kota_asal" name="kota_asal" class="form-control chosen-select">
                                                    <option value="">- Pilih Kota -</option>
                                                    <option value="">Malang</option>
                                                    <option value="">Malang (ABD Saleh)</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Kota Tujuan</label>
                                                <select id="kota_tujuan" name="kota_tujuan" class="form-control chosen-select">
                                                    <option value="">- Pilih Kota -</option>
                                                    <option value="">Malang</option>
                                                    <option value="">Malang (ABD Saleh)</option>
                                                    <option value="">Surabaya</option>
                                                    <option value="">Surabaya (Juanda)</option>
                                                    <option value="">Sidoarjo</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tanggal Berangkat</label>
                                                <input type="text" class="form-control" id="tanggal_berangkat" placeholder="Tanggal Berangkat">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Jam Berangkat</label>
                                                <input type="text" class="form-control" id="jam_berangkat" placeholder="Jam Berangkat">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-block">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="map-vendor">
            </div>
            <!--
            <div class="bs-component">
                <div class="panel panel-default">
                    <div class="panel-body" style="background-color: #1ba0e2;">
                        <form id="filter" action="/filter" method="post">
                            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="checkbox" value="Yes" name="airConditioning">
                                        <label style="color:#fff">AC</label>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="checkbox" value="Yes" name="airConditioning">
                                        <label style="color:#fff">Charger</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" style="margin-bottom: 0px;">
                                        <input type="checkbox" value="Yes" name="airConditioning">
                                        <label style="color:#fff">Wifi</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>-->
        </div>
        <div class="col-lg-8" id="main">
            <div class="bs-component">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row singlearmada">
                            <div class="col-md-2 logo-box">
                                <div class="logo">
                                    Logo
                                </div>
                            </div>
                            <div class="col-md-3 name-box">
                                <span class="name">Travel Vendor</span>
                                <!-- <span class="rating">* * * * *</span> -->
                            </div>
                            <div class="col-md-2 clock-box">
                                <i class="fa fa-clock-o"></i> 03:00
                                <br>
                            </div>
                            <div class="col-md-3 harga-box">Rp. 100.000,00</div>
                            <div class="col-md-2 button-box">
                                <a href="/cari/detail-vendor" class="btn btn-default travelcar-pesan" style="padding: 5px 10px;">Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row singlearmada">
                            <div class="col-md-2 logo-box">
                                <div class="logo">
                                    Logo
                                </div>
                            </div>
                            <div class="col-md-3 name-box">
                                <span class="name">Travel Vendor</span>
                                <!-- <span class="rating">* * * * *</span> -->
                            </div>
                            <div class="col-md-2 clock-box">
                                <i class="fa fa-clock-o"></i> 03:00
                                <br>
                            </div>
                            <div class="col-md-3 harga-box">Rp. 100.000,00</div>
                            <div class="col-md-2 button-box">
                                <a class="btn btn-default travelcar-pesan" style="padding: 5px 10px;">Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row singlearmada">
                            <div class="col-md-2 logo-box">
                                <div class="logo">
                                    Logo
                                </div>
                            </div>
                            <div class="col-md-3 name-box">
                                <span class="name">Travel Vendor</span>
                                <!-- <span class="rating">* * * * *</span> -->
                            </div>
                            <div class="col-md-2 clock-box">
                                <i class="fa fa-clock-o"></i> 03:00
                                <br>
                            </div>
                            <div class="col-md-3 harga-box">Rp. 100.000,00</div>
                            <div class="col-md-2 button-box">
                                <a class="btn btn-default travelcar-pesan" style="padding: 5px 10px;">Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row singlearmada">
                            <div class="col-md-2 logo-box">
                                <div class="logo">
                                    Logo
                                </div>
                            </div>
                            <div class="col-md-3 name-box">
                                <span class="name">Travel Vendor</span>
                                <!-- <span class="rating">* * * * *</span> -->
                            </div>
                            <div class="col-md-2 clock-box">
                                <i class="fa fa-clock-o"></i> 03:00
                                <br>
                            </div>
                            <div class="col-md-3 harga-box">Rp. 100.000,00</div>
                            <div class="col-md-2 button-box">
                                <a href="" class="btn btn-default travelcar-pesan" style="padding: 5px 10px;">Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row singlearmada">
                            <div class="col-md-2 logo-box">
                                <div class="logo">
                                    Logo
                                </div>
                            </div>
                            <div class="col-md-3 name-box">
                                <span class="name">Travel Vendor</span>
                                <!-- <span class="rating">* * * * *</span> -->
                            </div>
                            <div class="col-md-2 clock-box">
                                <i class="fa fa-clock-o"></i> 03:00
                                <br>
                            </div>
                            <div class="col-md-3 harga-box">Rp. 100.000,00</div>
                            <div class="col-md-2 button-box">
                                <a class="btn btn-default travelcar-pesan" style="padding: 5px 10px;">Pesan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<?php
$this->registerJsFile('@web/dev/js/pagejs/cari.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>