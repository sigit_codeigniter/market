<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TemplateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "dev/css/bootstrap.css",
        "dev/css/chosen.css",
        "dev/css/datepicker.fixes.css",
        "dev/css/custom.min.css",
        "dev/css/custom.css",
    ];
    public $js = [
        "https://code.jquery.com/jquery-1.10.2.min.js",
        "http://code.jquery.com/ui/1.10.3/jquery-ui.js",
        "dev/js/bootstrap.min.js",
        "dev/js/chosen.jquery.js",
        "http://maps.googleapis.com/maps/api/js?v=3.exp",
        "dev/js/bootstrap-datepicker.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );



}
