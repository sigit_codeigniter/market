<?php

namespace backend\controllers;

use Yii;
use backend\models\Kota;
use backend\models\KotaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * KotaController implements the CRUD actions for Kota model.
 */
class KotaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kota models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KotaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kota model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Kota model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kota();
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Save or Update an existing Kota model.
     * If id is empty then save else updat existing Kota model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAddkota()
    {
        $model = new Kota();
        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->post();
            $simpanKota = $model->simpanKota($data['Kota']);
            if($data['Kota']['id'] == ""){
                $pesan = 1;
            }
            else{
                $pesan = 2;
            }
            return json_encode($pesan);
        }
    }


    /**
     * Updates an existing Kota model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Finds the Kota model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kota the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kota::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Fetch json Orders
     * @return mixed
     */
    public function actionData(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $req = $_REQUEST;
        $orders  = new Kota();
        $request = Yii::$app->request;
        $search = $request->get('sSearch');
        $pageNumber = (integer) $request->get('iDisplayStart');
        $iDisplayLength = $request->get('iDisplayLength');
        $column = !empty($orders->setTable($req['iSortCol_0'])) ? $orders->setTable($req['iSortCol_0']) : 'id';
        $action = !empty($req['sSortDir_0']) ? $req['sSortDir_0'] : 'asc';
        $json = $orders->dataTable($search,$pageNumber,$iDisplayLength,$column,$action);
        return $json;
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Kota model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $kota = new Kota();
        $data = $request->post();
        $id = $data['id'];
        $delete = $kota->softDelete($id);
        if ($request->isAjax) {
            return $delete;
        }
        else{
            return $this->redirect(['index']);
        }
    }

}
