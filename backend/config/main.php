<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [

        // 'assetManager' => [
        //  'appendTimestamp' => true,
        //  'bundles' => [
        //    'yii\web\JqueryAsset' => [
        //      'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
        //      'js' => [
        //        'jquery.min.js'
        //      ]
        //    ],
        //    'yii\bootstrap\BootstrapAsset' => [
        //      'css' => [
        //        'css/bootstrap.min.css'
        //      ],
        //      'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
        //      'js' => [
        //        'js/bootstrap.min.js'
        //      ]
        //    ]
        //  ]
        // ],

        'urlManager' => [
          'class' => 'yii\web\UrlManager',
          'showScriptName' => false,
          'enablePrettyUrl' => true
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
