<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%kota}}".
 *
 * @property string $id
 * @property string $kode
 * @property string $nama
 * @property integer $status
 *
 * @property Kecamatan[] $kecamatans
 * @property TrvJurusanAsal[] $trvJurusanAsals
 * @property TrvJurusanTujuan[] $trvJurusanTujuans
 */
class Kota extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kota}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'nama'], 'required'],
            [['status'], 'integer'],
            [['kode'], 'string', 'max' => 15],
            [['nama'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'status' => '1=tampil,2=delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKecamatans()
    {
        return $this->hasMany(Kecamatan::className(), ['id_kota' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrvJurusanAsals()
    {
        return $this->hasMany(TrvJurusanAsal::className(), ['id_kota' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrvJurusanTujuans()
    {
        return $this->hasMany(TrvJurusanTujuan::className(), ['id_kota' => 'id']);
    }

    /**
     * @inheritdoc
     * @return KotaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KotaQuery(get_called_class());
    }

    public function setTable($column){
        $data = Kota::getTableSchema()->getColumnNames();
        $check = array_search($column,$data);
        if($check != false){
            return $data[$check];
        }
        else{
            return '';
        }
    }

    public function dataTable($search,$pageNumber,$iDisplayLength,$columnSort, $sortAct){
        $count= Kota::find()
            ->orFilterWhere(['like', 'nama', $search])
            ->andWhere("status!=2")
            ->count();
        $data = array();
        $findData = Kota::find()
            ->orFilterWhere(['like', 'nama', $search])
            ->andWhere("status!=2")
            ->orderBy($columnSort.' '.$sortAct)
            ->limit($iDisplayLength)
            ->offset($pageNumber)
            ->asArray()
            ->all();

        foreach ($findData as $value) {
            $data[] = array(
                'kode' => $value['kode'],
                'nama' => $value['nama'],
                'action' => '<a data-id="'.$value['id'].'" id="btn_delete" class="delete btn btn-sm btn-default"><i class="glyphicon glyphicon-remove"></i> Hapus</a><a data-id="'.$value['id'].'" id="btn_update" class="edit btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i> Edit</a>',
            );
        }

        $json = array(
            'iTotalRecords' => $count,
            'iTotalDisplayRecords' => $count,
            'aaData' => $data
        );

        return $json;

    }

    public function simpanKota($data){
        if($data['id'] == ""){
            $kota = new Kota;
        }
        else{
            $kota = Kota::findOne($data['id']);
        }
        $kota->kode = $data['kode'];
        $kota->nama = $data['nama'];
        $kota->save();
    }

    public function getOne($id){
        $model = Kota::find()->where(['id' => $id])->count();
        if($model == 1){
            return Kota::find()->where(['id' => $id])->asArray()->one();
        }
        else{
            return array();
        }
    }

    public function softDelete($id){
        $model = $this->getOne($id);
        if(count($model)){
            $order = Kota::findOne($id);
            $order->status = 2;
            $order->save(false);
            return 'Proses hapus data sukses';
        }
        else{
            return 'Proses hapus data gagal';
        }
    }
}
