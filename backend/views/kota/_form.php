<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Kota */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="kota-form">
    <?php $form = ActiveForm::begin([
        'id' => 'addKota-form',
        'action' => ['create'],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true]);
    ?>
    <?= $form->field($model, 'id')->hiddenInput(['class' => 'form-control'])->label(false) ?>
    <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-warning btn-sm', 'name' => 'simpan-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>