<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Parselday Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
      <div class="panel-body">
            <?php
            // $form = ActiveForm::begin(['id' => 'updatePassword', 'options' => ['class' => 'form-horizontal',
            // 'onsubmit' => 'return editProfile(this);']]);
            ?>

            <div class="alert alert-info responseAddDistanceprice" style="display:none;" role="alert"></div>
            <br />
            <?php $form = ActiveForm::begin([
                'id' => 'adddistanceprice-form',
                'action' => ['create'],
                'enableAjaxValidation' => false,
                'enableClientValidation' => true]);
            ?>
            <?= $form->field($model, 'id')->hiddenInput(['class' => 'form-control'])->label(false) ?>
            <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
            <br />
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-warning btn-block btn-lg', 'name' => 'simpan-button']) ?>
            <?php ActiveForm::end(); ?>
      </div>
</div>
<?php
$this->registerJS(<<< EOT_JS
$(function() {
    $(".must_number").keyup(function () {
          this.value = this.value.replace(/[^0-9\.]/g,'');
    });
});
EOT_JS
);
?>
