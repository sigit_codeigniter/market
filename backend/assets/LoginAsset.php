<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "dev/css/bootstrap.min.css",
        "dev/css/font-awesome.min.css",
        "dev/css/ionicons.min.css",
        "dev/css/AdminLTE.min.css",
        "dev/css/page/Login.css",
        "dev/plugins/iCheck/square/blue.css",
    ];
    public $js = [
        "dev/js/bootstrap.min.js",
        "dev/plugins/iCheck/icheck.min.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );

}
