<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TemplateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "dev/css/bootstrap.min.css",
        "dev/css/font-awesome.min.css",
        "dev/css/ionicons.min.css",
        "dev/css/jquery-jvectormap-1.2.2.css",
        "dev/datatable/css/dataTables.bootstrap.css",
        "dev/css/AdminLTE.min.css",
        "dev/css/_all-skins.min.css",
    ];
    public $js = [
        "dev/js/bootstrap.min.js",
        "dev/js/fastclick.min.js",
        "dev/js/app.min.js",
        "dev/js/jquery.sparkline.min.js",
        "dev/js/jquery-jvectormap-1.2.2.min.js",
        "dev/js/jquery-jvectormap-world-mill-en.js",
        "dev/datatable/js/jquery.dataTables.min.js",
        "dev/datatable/js/dataTables.bootstrap.min.js",
        "dev/js/jquery.slimscroll.min.js",
        "dev/js/noty/noty.min.js",
        "dev/js/Chart.min.js",
        "dev/js/demo.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );

}
