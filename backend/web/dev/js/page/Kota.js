jQuery.fn.dataTableExt.oApi.fnPagingInfo = function (oSettings) {
    return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": oSettings._iDisplayLength === -1 ?
            0 : Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
    };
};

$(document).ready(function () {
    try {
        var _elem = '<button type="button" class="btn btn-sm  btn-flat btn-default btn-flat" id="add"><i class="glyphicon glyphicon-plus"></i> Tambah</button>';
        var dt = $("#kota_table").DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sort": "position",
            "bStateSave": false,
            "iDisplayLength": 10,
            "iDisplayStart": 0,
            "fnDrawCallback": function () {
            },
            "sAjaxSource": "/kota/data",
            "aoColumns": [
                {"mData": "kode"},
                {"mData": "nama"},
                {"mData": "action"},
            ]
        });
        $("#kota_table_filter").prepend(_elem);
        $(document).delegate("#add", "click", function(){
            $("#titleModal").empty();
            $("#titleModal").append('Tambah Kota');
            $("#actionModal").modal('show');
            $.get("/kota/create", function (data) {
                $(".modal-body").html(data);
            });
            return false;
        });
        $(document).delegate(".edit", "click", function(){
            $("#titleModal").empty();
            $("#titleModal").append('Edit Distance Price');
            $("#actionModal").modal('show');
            var id = $(this).data("id");
            $.get("/kota/edit",{ id: id}, function (data) {
                $(".modal-body").html(data);
            });
            return false;
        });
        $(document).delegate(".delete", "click", function(){
            var id = $(this).data("id");
            $.post("/kota/delete",{ id: id}, function (data) {
                var x = noty({
                    text        : "Konfigurasi Berhasil Dihapus",
                    type        : 'information',
                    dismissQueue: true,
                    killer      : true,
                    timeout     : 3000,
                    layout      : 'bottomRight',
                    theme       : 'defaultTheme'
                });

                $.noty.closeAll();
                $("#kota_table").DataTable().ajax.reload();
            });
            return false;
        });
        $('body').on('beforeSubmit', 'form#addKota-form', function () {
            var form = $(this);
            if(form.find('.has-error').length) {
                return false;
            }
            $.ajax({
                url: '/kota/addkota',
                type: 'post',
                data: form.serialize(),
                success: function(data) {
                    if(data == 1){
                        var pesan = 'Kota Berhasil Ditambahkan';
                    }
                    else{
                        var pesan = 'Kota Berhasil Diubah';
                    }
                    var x = noty({
                        text        : pesan,
                        type        : 'information',
                        dismissQueue: true,
                        killer      : true,
                        timeout     : 3000,
                        layout      : 'bottomRight',
                        theme       : 'defaultTheme'
                    });

                    $.noty.closeAll();
                    $("#actionModal").modal('hide');
                    $("#kota_table").DataTable().ajax.reload();
                    return false;
                }
            });
            return false;
        });

    }catch (err){
        console.log(err.message)
    }
});
